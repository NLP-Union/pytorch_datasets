# Pytorch-自带数据集

因码云文件大小限制，现将所有数据集放在百度网盘中,请按需下载即可

##　如何下载
链接：https://pan.baidu.com/s/154eNfGRYPIxzsVSOnG8H3A 
提取码：xda0 

## 如何使用
```
mnist数据集: 
datasets.MNIST(root='.../pytorch_datasets/mnist', train=True, transform=transforms.ToTensor())
```